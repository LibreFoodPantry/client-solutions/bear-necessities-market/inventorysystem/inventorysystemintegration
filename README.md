# Inventory System Integration

DATE UPDATED: November, 11th, 2024

## Overview

The purpose of this project is to provide a working model of Integration between the Inventory System Frontend and the Inventory System Backend for the Bear Necessities Market.

# Status

Integration is currently up and running, using the instructions below.

## Installation Instructions

first open [docker destop](https://www.docker.com/products/docker-desktop/)(<-wesite link) on your device use the code dropdown selecting the http option and then cloning it locally. Navigate into the project's root folder and use `docker-compose up` to run the system or use the commands in the command folders located in the other sections.

## Usage Instructions

User input and output is performed through a GUI hosted on port 10300. Connect to `localhost:10300` to interact with the system.

## Tools

- [RabbitMQ](https://www.rabbitmq.com/)

All other tools used in this project can be found in the [Backend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/inventorysystem/backend) readme and [Frontend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/inventorysystem/frontend) readme.

## Bin

Git command that squashes all commits can be found in the [bin](/bin) this folder is now the command folder.

## License

[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html)
